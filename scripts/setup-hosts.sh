#!/bin/bash

echo "Creating hosts file"
cat > /etc/hosts <<EOF
192.168.5.2 nodemaster
192.168.5.3 node1
EOF

#!/usr/bin/python2
#-*- coding: utf-8 -*-

"""@author: walter
"""

import ntpath
import numpy as np
import matplotlib.pyplot as plt
import glob
#from moviepy.editor import VideoFileClip
from collections import deque
from sklearn.utils.linear_assignment_ import linear_assignment
import cv2
import math
import simplejson
import uuid

import helpers
import SelectStreets
import detector
import tracker
import copy

import time
import pylab
import structures

import sys, os
os.environ["PYSPARK_PYTHON"] = "python2"
os.environ['PYTHONPATH'] = ':'.join(sys.path)

print("Python Version: ", sys.version)


# ----hadoop-------
import hdfs
from hdfs.client import InsecureClient
import pyspark
from pyspark import SparkContext
conf = pyspark.SparkConf()
#conf=pyspark.SparkConf().setMaster("local").setAppName("RatingHistogram")

master_url = 'http://192.168.5.2:9870'
sc = SparkContext(appName="hadoop")
#sc = SparkContext(conf=conf)
#sc = SparkContext('local', appName="hadoop")

print("Despues de contexto ")

#----------------------------------------

Polynomial = np.polynomial.Polynomial

# Global variables to be used by funcitons of VideoFileClop
frame_count = 0 # frame counter

max_age = 5  # no.of consecutive unmatched detection before
             # a track is deleted

min_hits =1  # no. of consecutive matches needed to establish a track

scale=146/99

frameRate =30

pathVideo=""

streetList=[]

tracker_list =[] # list for trackers
tracker_list_10f=[]
# list for track ID
track_id_list= deque(['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K','L','M',
                        'N','O','P','Q','R','S','T','U','V','W','X','Y','Z'])

debug = False #True
withImages=False
withDarknet = True
velConstant = True # cambiar tambien en el tracker.py


#Remove logs
pathLogs="/tmp/logs.txt"
if os.path.isfile(pathLogs):
    os.remove(pathLogs)



def main():

     print("**************************************** start main")
     global sc	
     #sc = SparkContext(appName="hadoop")
     #sc = SparkContext(appName="hadoop")
     #sc = SparkContext(appName="hadoop", pyFiles=['helpers.py', 'SelectStreets.py', 'detector.py', 'tracker.py', 'structures.py'])
     #sc.addFile("/home/hadoop/carcollision2/pythonPrograms/libdarknet.so") 
     client = InsecureClient(master_url, user='hadoop')
     paths = client.list('vids')
     filepaths = ['vids/{}'.format(x) for x in paths]

     print("filepaths", filepaths)
     print("**************************************** before parallelize")
     printToLog("**************************************** before parallelize")
     results = sc.parallelize(filepaths).map(process_image).collect()
     #results = sc.parallelize(filepaths).map(process_image).sum()
     print("**************************************** after parallelize")
     #print(results, type(results))
     #print(int(results))
     #register_count(int(results))


def printToLog(message):

     file = open("/tmp/logs.txt","a")
     file.write(message)
     file.write("\n")
     file.close()

def path_leaf(path):
    head, tail = ntpath.split(path)
    return tail or ntpath.basename(head)


#def process_image(path):
#    printToLog("**************************************** inicio Process_image")
#    return 5 


def process_image(path):
    
    printToLog("**************************************** inicio Process_image")
     
    det = detector.CarDetector(withDarknet)
    printToLog(path)	
    fileName=path_leaf(path)
    printToLog(fileName)

    if fileName.endswith('.mp4'):
        fileName = fileName[:-4]

    #destPath='/tmp/'+fileName+'.avi'

    client = InsecureClient(master_url, user='hadoop')
    #client.download(path, destPath)

    random_ = str(uuid.uuid1())
    random = '/tmp/ori-{}.mp4'.format(random_)
    client.download(path, random)

    save = '/tmp/rs-{}.avi'.format(random_)

    printToLog("Antes de car detector")	
    
    #det = detector.CarDetector(withDarknet)
    global pathVideo    
    cwd= os.getcwd()
    os.chdir(cwd)
    os.chdir("../../..")

    printToLog("Antes de videocapture")	
    cap = cv2.VideoCapture(random)
    pathVideo=fileName

    printToLog("Antes de video writer fourcc")	
    printToLog(save)	
    # Define the codec and create VideoWriter object
    fourcc = cv2.VideoWriter_fourcc('M','J','P','G')
    printToLog("Antes de video writer out")	
    out = cv2.VideoWriter(save,fourcc, 30.0, (320,240))

    printToLog("Despues de video writer")	
    while(cap.isOpened()):
        printToLog("dentro del while")	
        ret, frame = cap.read()
        if ret==True:
            #frame = cv2.flip(frame,0)
            printToLog("antes de pipeline")	
            frame =pipeline(frame,det)

            printToLog("Despues de cada frame")	
            # write the flipped frame
            out.write(frame)

            #cv2.imshow('frame',frame)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
        else:
            break

    printToLog("Despues de while")	
    # Release everything if job is finished
    cap.release()
    out.release()
    cv2.destroyAllWindows()

    #return 5

def assign_streets_to_trackers(trk,maskcar,box_car):
    iooalist=[]
    for idx,maskstreet in enumerate(streetList):
        iooa=rate_iooa(maskstreet.mask,maskcar,box_car)
        iooalist.append(iooa)

    if len(iooalist)!=0:
        maxiooa = max(iooalist)
        maxiooas=sorted( [(x,i) for (i,x) in enumerate(iooalist)], reverse=True )[:3]
        if len(iooalist)==1:
            if maxiooa>0.0:
                street=iooalist.index(maxiooa)
                trk.street=streetList[street]
                if len(trk.histoStreet)>0:
                    if trk.histoStreet[-1]=="NaN":
                        trk.histoStreet[-1]=streetList[street].id
                    else:
                        trk.histoStreet.append(streetList[street].id)
                else:
                    trk.histoStreet.append(streetList[street].id)
            #     #print "calle: ",street," carro: ",trk.id
            # else:
        elif len(iooalist)>1:
            maxiooa_1=maxiooas[0]
            maxiooa_2=maxiooas[1]
            file=open("logs.txt","a")
            file.write("-------------------------------------\n")
            #file.write("Todos: \n".join(maxiooas))
            simplejson.dump(maxiooas, file)
            file.write("max 1: %f\n" %maxiooa_1[0])
            file.write("idx 1: %f\n" %maxiooa_1[1])
            file.write("max 2: %f\n" %maxiooa_2[0])
            file.write("idx 2: %f\n" %maxiooa_2[1])
            file.write("-------------------------------------\n")
            file.close()

            if maxiooa_1[0]>0.7 and maxiooa_2[0]>=0.1:
                if streetList[maxiooa_1[1]].direction=="V" and streetList[maxiooa_2[1]].direction=="V" and trk.direction=="V":
                    street=maxiooa_1[1]
                    trk.street=streetList[street]
                    if len(trk.histoStreet)>0:
                        if trk.histoStreet[-1]=="NaN":
                            trk.histoStreet[-1]=streetList[street].id
                        else:
                            trk.histoStreet.append(streetList[street].id)
                    else:
                        trk.histoStreet.append(streetList[street].id)
                    #print "calle: ",street," carro: ",trk.id
                elif streetList[maxiooa_1[1]].direction=="H" and streetList[maxiooa_2[1]].direction=="V" and trk.direction=="V":
                    street=maxiooa_2[1]
                    trk.street=streetList[street]
                    if len(trk.histoStreet)>0:
                        if trk.histoStreet[-1]=="NaN":
                            trk.histoStreet[-1]=streetList[street].id
                        else:
                            trk.histoStreet.append(streetList[street].id)
                    else:
                        trk.histoStreet.append(streetList[street].id)
                elif streetList[maxiooa_1[1]].direction=="H" and streetList[maxiooa_2[1]].direction=="V" and trk.direction=="H":
                    street=maxiooa_1[1]
                    trk.street=streetList[street]
                    if len(trk.histoStreet)>0:
                        if trk.histoStreet[-1]=="NaN":
                            trk.histoStreet[-1]=streetList[street].id
                        else:
                            trk.histoStreet.append(streetList[street].id)
                    else:
                        trk.histoStreet.append(streetList[street].id)
                elif streetList[maxiooa_1[1]].direction=="V" and streetList[maxiooa_2[1]].direction=="H" and trk.direction=="V":
                    street=maxiooa_1[1]
                    trk.street=streetList[street]
                    if len(trk.histoStreet)>0:
                        if trk.histoStreet[-1]=="NaN":
                            trk.histoStreet[-1]=streetList[street].id
                        else:
                            trk.histoStreet.append(streetList[street].id)
                    else:
                        trk.histoStreet.append(streetList[street].id)
                elif streetList[maxiooa_1[1]].direction=="V" and streetList[maxiooa_2[1]].direction=="H" and trk.direction=="H":
                    street=maxiooa_2[1]
                    trk.street=streetList[street]
                    if len(trk.histoStreet)>0:
                        if trk.histoStreet[-1]=="NaN":
                            trk.histoStreet[-1]=streetList[street].id
                        else:
                            trk.histoStreet.append(streetList[street].id)
                    else:
                        trk.histoStreet.append(streetList[street].id)
                elif trk.direction=="":
                    street=maxiooa_1[1]
                    trk.street=streetList[street]
                    if len(trk.histoStreet)>0:
                        if trk.histoStreet[-1]=="NaN":
                            trk.histoStreet[-1]=streetList[street].id
                        else:
                            trk.histoStreet.append(streetList[street].id)
                    else:
                        trk.histoStreet.append(streetList[street].id)
                else:
                    streetNaN=copy.copy(streetList[0])
                    streetNaN.id=30
                    trk.street=streetNaN
                    trk.histoStreet.append("NaN")

            elif maxiooa_1[0]>maxiooa_2[0] and maxiooa_2[0]<=0.1 and maxiooa_2[0]>=0.0 and maxiooa_1[0]>0.3:
                street=maxiooa_1[1]
                trk.street=streetList[street]
                if len(trk.histoStreet)>0:
                    if trk.histoStreet[-1]=="NaN":
                        trk.histoStreet[-1]=streetList[street].id
                    else:
                        trk.histoStreet.append(streetList[street].id)
                else:
                    trk.histoStreet.append(streetList[street].id)
                #print "calle: ",street," carro: ",trk.id
            else:
                streetNaN=copy.copy(streetList[0])
                streetNaN.id=30
                trk.street=streetNaN
                trk.histoStreet.append("NaN")
                #print "no encotrada"
        else:

            streetNaN=copy.copy(streetList[0])
            streetNaN.id=30
            trk.street=streetNaN
            trk.histoStreet.append("NaN")
            #print "no encotrada"



def rate_iooa(maskstreet,maskcar,box_car):
    areaObj=cv2.contourArea(box_car)
    mas=maskstreet*maskcar
    edged = cv2.Canny(mas, 5,80)
    _, contours, _=cv2.findContours(edged, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
    #cv2.drawContours(self.im, contours, -1, (0,255,0),1)
    if len(contours)>0:
        areaInter = cv2.contourArea(contours[0])
    else:
        areaInter=0
    iooa=areaInter/areaObj
    return iooa

def box_iou2(a, b):
    
    #Helper funciton to calculate the ratio between intersection and the union of
    #two boxes a and b
    #a[0], a[1], a[2], a[3] <-> left, up, right, bottom

    w_intsec = np.maximum (0, (np.minimum(a[2], b[2]) - np.maximum(a[0], b[0])))
    h_intsec = np.maximum (0, (np.minimum(a[3], b[3]) - np.maximum(a[1], b[1])))
    s_intsec = w_intsec * h_intsec
    s_a = (a[2] - a[0])*(a[3] - a[1])
    s_b = (b[2] - b[0])*(b[3] - b[1])

    return float(s_intsec)/(s_a + s_b -s_intsec)

def car_direction(trk,img):

    centroids=trk.centroids
    if len(centroids)<10:
        centroid1=centroids[0]
        centroid2=centroids[-1]
    else:
        centroid1=centroids[-9]
        centroid2=centroids[-1]

    cx1=centroid1[0]
    cy1=centroid1[1]
    cx2=centroid2[0]
    cy2=centroid2[1]

    cv2.line(img,(int(cx1),int(cy1)),(int(cx2),int(cy2)), 255, 2)

    radians=math.atan2((cx1-cx2),(cy2-cy1)) # (y,x)=(y/x)
    angle=math.degrees(radians)

    if math.sin(radians)!=0:
        m=-math.cos(radians)/math.sin(radians)
    else:
        m=200


    if angle<0:
        angle=360+angle

    trk.angleDirection.append(angle)

    if angle<=35 and angle>=325:
        trk.direction="V"
    elif angle<=247 and angle>=130:
        trk.direction="V"
    elif angle<325 and angle>247:
        trk.direction="H"
    elif angle<130 and angle>35:
        trk.direction="H"


    return img

def assign_detections_to_trackers(trackers, detections, iou_thrd = 0.3):
    #From current list of trackers and new detections, output matched detections,
    #unmatchted trackers, unmatched detections.

    IOU_mat= np.zeros((len(trackers),len(detections)),dtype=np.float32)
    for t,trk in enumerate(trackers):
        #trk = convert_to_cv2bbox(trk)
        for d,det in enumerate(detections):
         #   det = convert_to_cv2bbox(det)
            IOU_mat[t,d] = box_iou2(trk,det)

    # Produces matches
    # Solve the maximizing the sum of IOU assignment problem using the
    # Hungarian algorithm (also known as Munkres algorithm)

    matched_idx = linear_assignment(-IOU_mat)

    unmatched_trackers, unmatched_detections = [], []
    for t,trk in enumerate(trackers):
        if(t not in matched_idx[:,0]):
            unmatched_trackers.append(t)

    for d, det in enumerate(detections):
        if(d not in matched_idx[:,1]):
            unmatched_detections.append(d)

    matches = []

    for m in matched_idx:
        if(IOU_mat[m[0],m[1]]<iou_thrd):
            unmatched_trackers.append(m[0])
            unmatched_detections.append(m[1])
        else:
            matches.append(m.reshape(1,2))

    if(len(matches)==0):
        matches = np.empty((0,2),dtype=int)
    else:
        matches = np.concatenate(matches,axis=0)

    return matches, np.array(unmatched_detections), np.array(unmatched_trackers)

def calculate_centroid(xx,tmp_trk):
    cx=xx[1]+((xx[3]-xx[1])/2)
    cy=xx[0]+((xx[2]-xx[0])/2)
    centroid=np.array([cx,cy])

    if len(tmp_trk.centroids)>10:
        centroid_old=tmp_trk.centroids[len(tmp_trk.centroids)-9]
    else:
        centroid_old=tmp_trk.centroids[len(tmp_trk.centroids)-1]

    distancePix=np.sqrt(sum((centroid-centroid_old)**2))

    success=0
    if distancePix>0.9:
        tmp_trk.centroids.append(centroid)
        tmp_trk.frameNum.append(frame_count)
        success=1

    return tmp_trk,success,distancePix

def calculate_velocity(tmp_trk,distancePix):


    distanceUni=distancePix*scale

    if len(tmp_trk.centroids)>11:
        framebefore=tmp_trk.frameNum[len(tmp_trk.frameNum)-10]
        timebefore=(np.float(tmp_trk.frameNum[len(tmp_trk.frameNum)-10])/np.float(frameRate))
    else:
        framebefore=tmp_trk.frameNum[len(tmp_trk.frameNum)-2]
        timebefore=(np.float(tmp_trk.frameNum[len(tmp_trk.frameNum)-2])/np.float(frameRate))

    timeTravel=(np.float(frame_count)/np.float(frameRate))-timebefore
    tmp_trk.velocity.append(distanceUni/timeTravel)
    file=open("logs.txt","a")
    file.write("-------------------------------------\n")
    file.write("distacia: %f\n" %distanceUni)
    file.write("tiempo: %f\n" %timeTravel)
    file.write("tiempo antes: %f\n" %timebefore)
    file.write("count: %i\n" %frame_count)
    file.write("count before %i\n" %framebefore)
    file.write("velocidad: %f\n" %tmp_trk.velocity[-1])
    file.write("-------------------------------------\n")
    file.close()


    return tmp_trk

def calculate_Distance2cross(imgray,xx,trk):
    if len(streetList)!=0:
         maskcar=np.zeros_like(cv2.cvtColor(imgray, cv2.COLOR_BGR2GRAY))
         box_car=np.array([[xx[1],xx[0]],[xx[3],xx[0]],[xx[3],xx[2]],[xx[1],xx[2]]])
         cv2.fillPoly(maskcar,[box_car],[20])
         assign_streets_to_trackers(trk,maskcar,box_car)
         if trk.histoStreet[-1]!="NaN":
             if len(trk.histoStreet)>1:
                if trk.histoStreet[-2] != trk.histoStreet[-1]:
                    trk.crossPoint=[]

             vectdis=[]
             for idx,cent in enumerate(trk.street.interseCentroids):
                  dist=np.sqrt(sum((cent-trk.centroids[-1])**2))
                  vectdis.append(dist)
                  if dist < 40.0:
                    trk.colorProb=[255,0,0]

             trk.crossPoint.append(vectdis)
    else:
        trk.crossPoint.append(200)
    return trk

def probability_of_collision(trk):
    dtvelinf=-30.0
    dtvelsup=0.0
    dtposxinf=0.0
    dtposxsup=10.0
    dtposyinf=0.0
    dtposysup=10.0
    dtdirinf=0.0
    dtdirsup=90.0
    dtcrossinf=0.0
    dtcrosssup=40
    centroid1=trk.centroids[-2]
    centroid2=trk.centroids[-1]
    delt_vel=float(trk.velocity[-1]-trk.velocity[-2])
    delt_posx=float(centroid2[0]-centroid1[0])
    delt_posy=float(centroid2[1]-centroid1[1])
    delt_dir=float(trk.angleDirection[-1]-trk.angleDirection[-2])
    VF,PF,DF,CF=0.0,0.0,0.0,0.0

    #if len(trk.histoStreet)>0
    if len(trk.histoStreet)>0 and trk.histoStreet[0]!="NaN":
        crossDif=np.array(trk.crossPoint[-1])
        delt_cross=crossDif.min()

    else:
        delt_cross=dtcrosssup+1
    #-----------------------------------------
    if dtvelinf<=delt_vel<=dtvelsup:
        VF=(1.0/dtvelinf)*delt_vel
    else:
        VF=0.0
    #-----------------------------------------

    if dtposxinf<=abs(delt_posx)<=dtposxsup:
        #PF=(-1.0/dtposxsup)*abs(delt_posx)+1
        PF=(1.0/dtposxsup)*abs(delt_posx)
    elif dtposyinf<=abs(delt_posy)<=dtposysup:
        #PF=(-1.0/dtposysup)*abs(delt_posy)+1
        PF=(1.0/dtposysup)*abs(delt_posy)
    else:
        PF=0.0
    #-----------------------------------------

    if dtdirinf<=abs(delt_dir)<=dtdirsup:
        #DF=(-1.0/dtdirsup)*abs(delt_dir)+1
        DF=(1.0/dtdirsup)*abs(delt_dir)
    else:
        DF=0.0
    #-----------------------------------------

    if dtcrossinf<=abs(delt_cross)<=dtcrosssup:
        CF=(-1.0/dtcrosssup)*abs(delt_cross)+1
    else:
        CF=0.0

    trk.probability.append((VF+PF+DF+CF)/4.0)

    file=open("logs.txt","a")
    file.write("-------------------------------------\n")
    file.write("car: %s\n" %trk.id)
    file.write("street: %i\n" %trk.street.id)
    file.write("angle 2: %f\n" %trk.angleDirection[-1])
    file.write("angle 1: %f\n" %trk.angleDirection[-2])
    file.write("direction: %s\n" %trk.direction)
    file.write("delta dir: %f\n" %delt_dir)
    file.write("probabity DF: %f\n" %DF)
    file.write("delta vel: %f\n" %delt_vel)
    file.write("velocidad1: %f\n" %trk.velocity[-2])
    file.write("velocidad2: %f\n" %trk.velocity[-1])
    file.write("probabity VF: %f\n" %VF)
    file.write("delta posx: %f\n" %delt_posx)
    file.write("delta posy: %f\n" %delt_posy)
    file.write("probabity PF: %f\n" %PF)
    file.write("delta cross: %f\n" %delt_cross)
    file.write("probabity CF: %f\n" %CF)
    file.write("probabity: %f\n" %trk.probability[-1])
    file.close()

    return trk



def pipeline(img,det):

    
#def pipeline(img,file):
    
    #Pipeline function for detection and tracking
    
    #global det
    global frame_count
    global tracker_list
    global tracker_list_10f
    global max_age
    global min_hits
    global track_id_list
    global debug
    global streetList
    global pathVideo

    frame_count+=1

    if (frame_count==1):
        stee=SelectStreets.Sel_Streets(img,pathVideo)
        stee.streets_rois()
        streetList=stee.streetList

    if withDarknet:
        if withImages:
            z_box = det.get_localizationYolo(img,file) # measurement
        else:
            printToLog("antes de yolo")	
            image=cv2.cvtColor(img,cv2.COLOR_BGR2RGB)
            cv2.imwrite("/tmp/image.jpg",image)
            printToLog("antes de localization yolo")	
            z_box = det.get_localizationYolo(img,'/tmp/image.jpg') # measurement
    else:
        z_box = det.get_localization(img) # measurement


    printToLog("despues de yolo")	
    img_dim = (img.shape[1], img.shape[0])
    #z_box = det.get_localization(img) # measurement
    if debug:
       print('Frame:', frame_count)

    x_box =[]
    if debug:
        for i in range(len(z_box)):
           img1= helpers.draw_box_label(img, z_box[i], box_color=(255, 0, 0))
           plt.imshow(img1)
        plt.show()

    if len(tracker_list) > 0:
        for trk in tracker_list:
            x_box.append(trk.box)



    matched, unmatched_dets, unmatched_trks \
    = assign_detections_to_trackers(x_box, z_box, iou_thrd = 0.3)#0.3
    if debug:
         print('Detection:', z_box)
         print('x_box: ', x_box)
         print('matched:', matched)
         print('unmatched_det:', unmatched_dets)
         print('unmatched_trks:', unmatched_trks)


    # Deal with matched detections
    if matched.size >0:
        for trk_idx, det_idx in matched:
            z = z_box[det_idx]
            z = np.expand_dims(z, axis=0).T
            tmp_trk= tracker_list[trk_idx]
            tmp_trk.kalman_filter(z)
            xx = tmp_trk.x_state.T[0].tolist()
            if velConstant:
                xx =[xx[0], xx[2], xx[4], xx[6]]
            else:
                xx =[xx[0], xx[3], xx[6], xx[9]]


            tracker_list_10f[trk_idx]=copy.copy(tmp_trk)
            x_box[trk_idx] = xx
            tmp_trk.box =xx
            tmp_trk.hits += 1


    # Deal with unmatched detections
    if len(unmatched_dets)>0:
        for idx in unmatched_dets:
            z = z_box[idx]
            z = np.expand_dims(z, axis=0).T
            tmp_trk = tracker.Tracker() # Create a new tracker
            if velConstant:
                x = np.array([[z[0], 0, z[1], 0, z[2], 0, z[3], 0]]).T
            else:
                x = np.array([[z[0], 0, 0, z[1], 0, 0, z[2], 0, 0, z[3], 0, 0]]).T
            tmp_trk.x_state = x
            tmp_trk.predict_only()
            xx = tmp_trk.x_state
            xx = xx.T[0].tolist()
            if velConstant:
                xx =[xx[0], xx[2], xx[4], xx[6]]
            else:
                xx =[xx[0], xx[3], xx[6], xx[9]]

            #-------fist time-------------------
            cx=xx[1]+((xx[3]-xx[1])/2)
            cy=xx[0]+((xx[2]-xx[0])/2)
            centroid=np.array([cx,cy])
            tmp_trk.centroids.append(centroid)
            tmp_trk.frameNum.append(frame_count)
            #------------------------------------------------

            tmp_trk.box = xx
            tmp_trk.id = track_id_list.popleft() # assign an ID for the tracker
            tmp_trk_10f=copy.copy(tmp_trk)
            tracker_list.append(tmp_trk)
            tracker_list_10f.append(tmp_trk_10f)
            x_box.append(xx)

    # Deal with unmatched tracks
    if len(unmatched_trks)>0:
        for trk_idx in unmatched_trks:
            tmp_trk = tracker_list[trk_idx]
            tmp_trk.no_losses += 1
            tmp_trk.predict_only()
            xx = tmp_trk.x_state
            xx = xx.T[0].tolist()
            if velConstant:
                xx =[xx[0], xx[2], xx[4], xx[6]]
            else:
                xx =[xx[0], xx[3], xx[6], xx[9]]

            tmp_trk.box =xx
            #tracker_list_10f[trk_idx]=copy.copy(tmp_trk)
            x_box[trk_idx] = xx


    # The list of tracks to be annotated
    good_tracker_list =[]
    good_tracker_list_10f=[]
    imgray=img.copy()
    for trk_idx,trk in enumerate(tracker_list):
    #for trk in tracker_list:
        if ((trk.hits >= min_hits) and (trk.no_losses <=max_age)):
             good_tracker_list.append(trk)
             good_tracker_list_10f.append(tracker_list_10f[trk_idx])

             #--------------collision features-----------------
             xx=trk.box

             trk,success,distancePix=calculate_centroid(xx,trk)
             if len(trk.frameNum)>0:
                trk=calculate_velocity(trk,distancePix)

             if success:
                 img=car_direction(trk,img)

                 if len(trk.angleDirection)>2:
                    trk=calculate_Distance2cross(imgray,xx,trk)
                    trk=probability_of_collision(trk)
             #--------------------------------------------------
             x_cv2 =trk.box

             if debug:
                 print('updated box:', x_cv2)
             img= helpers.draw_box_label(img, x_cv2,trk.box_color,trk.velocity,trk.id,trk.street,trk.colorProb,trk) # Draw the bounding boxes on the
             #img= helpers.draw_line_traker(img,trk.centroids,trk.box_color)
             img= helpers.draw_poliLine_traker(img,trk.centroids,trk.box_color)
                                             # images

    # Book keeping
    deleted_tracks = filter(lambda x: x.no_losses >max_age, tracker_list)

    for trk in deleted_tracks:
            track_id_list.append(trk.id)

    tracker_list = [x for x in tracker_list if x.no_losses<=max_age]
    tracker_list_10f= [x for x in tracker_list_10f if x.no_losses<=max_age]

    if debug:
       print('Ending tracker_list: ',len(tracker_list))
       print('Ending good tracker_list: ',len(good_tracker_list))

    return img


if __name__ == "__main__":
    main()

